# Z-Score service
[![python](https://img.shields.io/badge/python-3.9.5-ffdb66?style=flat&labelColor=255073)](https://www.python.org/)
[![fastapi](https://img.shields.io/badge/fastapi-0.70.0-212121?style=flat&labelColor=009485)](https://fastapi.tiangolo.com/)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![coverage](https://img.shields.io/badge/coverage-100-brightgreen)](https://github.com/nedbat/coveragepy)


## Getting started
Start service:
```
make build-docker
make run-docker
```
after this actions service with OpenAPI specification will be present here: http://127.0.0.1:8000/docs
where you can try to send report!

Run tests and show test coverage:
```
make build-docker
make prep-coverage-report
make show-coverage-report
```



### Project status: In progress...
###TODO:
1. CI/CD
2. Add files for deploy to cloud platform
3. Work more with test code quality packages (setup settings)
4. Add dynamic badges instead of static


## SRS:
Professor Edward Altman, the co-founder of WiserFunding, created the Z-Score formula for
predicting bankruptcy.
Z = 1.2X1 + 1.4X2 + 3.3X3 + 0.6X4 + 1.0X5

X1 = working_capital / total_assets
X2 = retained_earnings / total_assets
X3 = ebit / total_assets
X4 = equity / total_liabilities
X5 = sales / total_assets

Implement an API endpoint company/<country iso code>/<id> to accept PUT that expects a
JSON payload on financials for 5 years. 

PUT /company/gb/10149809
{“financials”: [
{“year”: 2020, “ebit”: 123.45, “equity”: 234.56, “retained_earnings”: 345.67, “sales”:
1234.56, “total_assets”: 345.67, “total_liabilities”: 456.78},
{“year”: 2019, “ebit”: 122.63, “equity”: 224.56, “retained_earnings”: 325.33, “sales”:
1214.99, “total_assets”: 325.04, “total_liabilities”: 426.78},
{“year”: 2018, “ebit”: 120.17, “equity”: 214.06, “retained_earnings”: 225.00, “sales”:
1204.01, “total_assets”: 305.11, “total_liabilities”: 426.78},
{“year”: 2017, “ebit”: 118.23, “equity”: 204.96, “retained_earnings”: 125.97, “sales”:
1200.00, “total_assets”: 290.75, “total_liabilities”: 426.78},
{“year”: 2016, “ebit”: 116.05, “equity”: 234.56, “retained_earnings”: 105.11, “sales”:
1010.82, “total_assets”: 250.13, “total_liabilities”: 426.78}]}

returns a JSON response for the scores where xxx are the Z-scores for those 5 years.
{“scores”: [{“year”: 2020, “zscore”: xxx}, {“year”: 2019, “zscore”: xxx}, {“year”: 2018,
“zscore”: xxx}, {“year”: 2017, “zscore”: xxx}, {“year”: 2016, “zscore”: xxx}]}

If you have time and then go further with extras such as;
• Additional endpoints to return previously generated reports
• Database models in postgres or sqlite, mysql etc.
• Build and deploy to free cloud tier on AWS, GCP, Azure etc.
• docker compose up to run locally
• API documentation and examples
• Use of pre-commit, black, mypy, isort etc.
• Test coverage of 100%
• CI/CD with Travis or similar
