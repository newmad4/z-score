build-docker:
	@$ docker-compose build

run-docker:
	@$ docker-compose up

run-tests:
	@$ docker-compose exec financial_backend pytest

type-lint:
	@$ docker-compose exec financial_backend mypy ./app ./tests

prep-coverage-report:
	@$ docker-compose exec financial_backend coverage run -m pytest

show-coverage-report:
	@$ docker-compose exec financial_backend coverage report -m

run-pre-commit:
	@$ pre-commit run --all-files
