import pytest
from starlette import status


@pytest.mark.asyncio
class TestRetrieveReport:
    """Test class for check retrieve detail information about report."""

    def test_retrieve_non_exist_report(self, api_client):
        response = api_client.get("z-score/api/v1/reports/1")
        assert response.status_code == status.HTTP_404_NOT_FOUND

    def test_retrieve_exist_report(self, api_client, report):
        response = api_client.get("z-score/api/v1/reports/1000")
        assert response.status_code == status.HTTP_200_OK
