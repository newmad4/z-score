import pytest
from starlette import status


@pytest.mark.asyncio
class TestListReports:
    """Test class for check get list of reports."""

    @pytest.fixture()
    def reports_url(self):
        return "z-score/api/v1/reports"

    def test_get_list_reports_without_reports(self, api_client, reports_url):
        response = api_client.get(reports_url)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "next_page": None,
            "previous_page": None,
            "current_page": 1,
            "total": 0,
            "data": [],
        }

    def test_get_list_reports_with_reports(self, api_client, report, reports_url):
        response = api_client.get(reports_url)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "next_page": None,
            "previous_page": None,
            "current_page": 1,
            "total": 1,
            "data": [{"id": 1000}],
        }
