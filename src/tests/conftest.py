import asyncio

import pytest
from starlette.testclient import TestClient
from tortoise import Tortoise

from app.main import app
from app.models import FinancialYear, Report

TEST_DATABASE_URL = "sqlite://:memory:"


async def init_db(db_url, create_db: bool = False, schemas: bool = False) -> None:
    """Initial database connection"""
    await Tortoise.init(db_url=db_url, modules={"models": ["app.models"]}, _create_db=create_db)
    if create_db:
        print(f"Database created! {db_url = }")
    if schemas:
        await Tortoise.generate_schemas()
        print("Success to generate schemas")


async def init(db_url: str = TEST_DATABASE_URL):
    await init_db(db_url, True, True)


@pytest.fixture(scope="module")
def event_loop():
    return asyncio.get_event_loop()


@pytest.fixture(scope="module", autouse=True)
async def initialize_tests():
    await init()
    yield
    await Tortoise._drop_databases()


@pytest.fixture(scope="module")
def api_client():
    client = TestClient(app)
    yield client


@pytest.fixture(scope="module")
def report_data():
    return {
        "financials": [
            {
                "year": 2020,
                "ebit": 123.45,
                "equity": 234.56,
                "retained_earnings": 345.67,
                "sales": 1234.56,
                "total_assets": 345.67,
                "total_liabilities": 345.67,
            },
        ]
    }


@pytest.fixture(scope="module")
async def report(report_data):
    report = await Report.create(id=1000)
    for financial_year_data in report_data["financials"]:
        await FinancialYear.create(report_id=report.id, **financial_year_data)
