import pytest
from fastapi import HTTPException
from starlette import status

from app.utils import validate_country_iso_code


@pytest.mark.asyncio
class TestSendReport:
    """Test class for check send report (create and retrieve report response)."""

    def test_send_report_invalid_payload(self, api_client):
        invalid_payload = {"title": "something"}

        response = api_client.put("z-score/api/v1/company/gb/10149809", json=invalid_payload)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    def test_send_report_valid_payload(self, api_client, report_data):
        response = api_client.put("z-score/api/v1/company/gb/10149809", json=report_data)
        assert response.status_code == status.HTTP_200_OK

    def test_send_report_invalid_country_code(self, api_client, report_data):
        response = api_client.put("z-score/api/v1/company/get/10149809", json=report_data)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    def test_send_report_invalid_report_id(self, api_client, report_data):
        response = api_client.put("z-score/api/v1/company/no/www", json=report_data)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    def test_send_report_invalid_method(self, api_client, report_data):
        response = api_client.post("z-score/api/v1/company/no/12", json=report_data)
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    async def test_validate_country_code(self):
        un_exist_country_iso_code = "lo"

        with pytest.raises(HTTPException):
            await validate_country_iso_code(country_iso_code=un_exist_country_iso_code)
