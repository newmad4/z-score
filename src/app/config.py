from dotenv import load_dotenv
from starlette.config import Config
from starlette.datastructures import Secret

project_name = "z-score"


class Settings:
    """Configuration class."""

    load_dotenv()
    config = Config()

    SERVICE_NAME = project_name
    INCLUDE_SCHEMA = config("INCLUDE_SCHEMA", cast=bool, default=True)
    CORS_ORIGINS = config("CORS_HOSTS", default="*")

    DEBUG = config("DEBUG", cast=bool, default=True)
    TESTING = config("TESTING", cast=bool, default=False)

    POSTGRES_USER = config("POSTGRES_USER", cast=str, default="z_score_user")
    POSTGRES_PASSWORD = config("POSTGRES_PASSWORD", cast=Secret, default="z_score_password")
    POSTGRES_HOST = config("POSTGRES_HOST", cast=str, default="localhost")
    POSTGRES_PORT = config("POSTGRES_PORT", cast=int, default=5432)
    POSTGRES_DB = config("POSTGRES_DB", cast=str, default="z_score_db")

    DATABASE_URL = config(
        "DATABASE_URL",
        default=f"postgres://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}",
    )
