import math

from tortoise.transactions import in_transaction

from app.models import FinancialYear, FinancialYear_Pydantic, Report, Report_Pydantic
from app.schemas import CompanyReportSchema, CompanyZscoreResultsSchema, ListReportsSchema


class ReportService:
    """Service for create/update/retrieve/get_list of financial reports.

    If Report was just created - create all financial_years instances.
    If report exist before -  update/create new one/ delete old financial_years -
    in reason of PUT method: create new instances or fully update existing object (and related objects).
    But in current version - I expect request with same data -so no need to update/delete/create financial_years.
    """

    @classmethod
    async def process_report(cls, report_id: int, all_reports_data: CompanyReportSchema) -> CompanyZscoreResultsSchema:
        financial_years = []
        async with in_transaction():
            report, report_was_created = await Report.get_or_create(id=report_id)
            for financial_year_data in all_reports_data.financials:
                financial_year = await FinancialYear(report_id=report_id, **financial_year_data.dict())
                financial_years.append(FinancialYear_Pydantic.from_orm(financial_year))
                if report_was_created:
                    await financial_year.save()

        return CompanyZscoreResultsSchema(scores=financial_years)

    @classmethod
    async def retrieve(cls, id: int) -> CompanyZscoreResultsSchema:
        report: Report = await Report.get(id=id).prefetch_related("financial_years")
        financial_years = await FinancialYear_Pydantic.from_queryset(report.financial_years.all())
        return CompanyZscoreResultsSchema(scores=financial_years)

    @classmethod
    async def paginated_list(cls, page: int = 1, size: int = 25) -> ListReportsSchema:
        total = await Report.all().count()

        pages = math.ceil(total / size)
        if page > pages:
            data = []
        else:
            reports = await Report.all().only("id").offset((page - 1) * size).limit(size)
            data = [Report_Pydantic.from_orm(report) for report in reports]

        return ListReportsSchema(
            total=total,
            current_page=page,
            next_page=page + 1 if page < pages else None,
            previous_page=page - 1 if page > 1 else None,
            data=data,
        )
