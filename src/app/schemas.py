from typing import Optional

from pydantic import BaseModel, Field


class FinancialReportSchema(BaseModel):
    year: int = Field(example=2020)
    ebit: float = Field(example=123.45)
    equity: float = Field(example=234.56)
    retained_earnings: float = Field(example=345.67)
    sales: float = Field(example=1234.56)
    total_assets: float = Field(
        example=345.67,
        description=(
            "All the assets of a company that are expected to be sold or used as a result of "
            "standard business operations over the next year."
        ),
    )
    total_liabilities: float = Field(
        example=345.67,
        description=(
            "company's short-term financial obligations that are due within one year or "
            "within a normal operating cycle"
        ),
    )


class CompanyReportSchema(BaseModel):
    financials: list[Optional[FinancialReportSchema]] = Field(
        example=[
            {
                "year": 2021,
                "ebit": 123.45,
                "equity": 234.56,
                "retained_earnings": 345.67,
                "sales": 1234.56,
                "total_assets": 345.67,
                "total_liabilities": 345.67,
            },
        ],
        description="Financial report for each year",
    )


class CompanyZscoreSchema(BaseModel):
    year: int = Field(example=2020)
    zscore: float = Field(
        example=2.95,
        description=(
            "Output of a credit-strength test that gauges a publicly traded manufacturing "
            "company's likelihood of bankruptcy."
        ),
    )


class CompanyZscoreResultsSchema(BaseModel):
    scores: list[CompanyZscoreSchema] = Field(
        example=[{"year": 2020, "zscore": 2.95}],
        description="Calculated Z-scores for each reports year",
        default=[],
    )


class TinyReportSchema(BaseModel):
    id: int = Field(example=1, description="Report ID")


class ListReportsSchema(BaseModel):
    next_page: Optional[int]
    previous_page: Optional[int]
    current_page: int
    total: int
    data: list[Optional[TinyReportSchema]] = Field(description="List of reports ID")
