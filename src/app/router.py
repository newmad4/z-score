from fastapi import APIRouter, Depends, Path, Query

from app.config import project_name
from app.schemas import CompanyReportSchema, CompanyZscoreResultsSchema, ListReportsSchema
from app.services import ReportService
from app.utils import validate_country_iso_code

report_router = APIRouter(prefix=f"/{project_name}/api/v1", tags=["Z-Score API"])


@report_router.put(
    "/company/{country_iso_code}/{id}",
    description="Create or update financial report",
    response_model=CompanyZscoreResultsSchema,
)
async def send_financial_report(
    payload: CompanyReportSchema,
    id: int = Path(..., description="Report ID"),
    country_iso_code: str = Depends(validate_country_iso_code),
) -> CompanyZscoreResultsSchema:
    return await ReportService.process_report(id, payload)


@report_router.get(
    "/reports",
    description="Return list of previously generated reports",
    response_model=ListReportsSchema,
)
async def get_list_reports(page: int = Query(1, gt=0), size: int = Query(25, gt=0)) -> ListReportsSchema:
    return await ReportService.paginated_list(page=page, size=size)


@report_router.get(
    "/reports/{id}",
    description="Retrieve detail information of previously generated report",
    response_model=CompanyZscoreResultsSchema,
)
async def get_detail_report(id: int = Path(..., description="Report ID")) -> CompanyZscoreResultsSchema:
    return await ReportService.retrieve(id)
