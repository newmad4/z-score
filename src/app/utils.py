import pycountry
from fastapi import HTTPException, Path
from starlette import status


async def validate_country_iso_code(
    *,
    country_iso_code: str = Path(..., description="Country ISO code (alpha_2)", regex=r"^[A-Za-z]{2}$"),
) -> None:
    if not pycountry.countries.get(alpha_2=country_iso_code.upper()):
        raise HTTPException(
            detail="Invalid alpha_2 country iso code",
            status_code=status.HTTP_400_BAD_REQUEST,
        )
