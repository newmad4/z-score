from tortoise import fields
from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise.models import Model


class Report(Model):
    id = fields.IntField(pk=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)

    class Meta:
        table = "report"


class FinancialYear(Model):
    id = fields.IntField(pk=True, generated=True)
    report = fields.ForeignKeyField("models.Report", related_name="financial_years")
    year = fields.IntField()
    ebit = fields.FloatField()
    equity = fields.FloatField()
    retained_earnings = fields.FloatField()
    sales = fields.FloatField()
    total_assets = fields.FloatField()
    total_liabilities = fields.FloatField()

    def working_capital(self) -> float:
        return self.total_assets - self.total_liabilities

    def zscore(self) -> float:
        return (
            1.2 * self.working_capital() / self.total_assets
            + 1.4 * self.retained_earnings / self.total_assets
            + 3.3 * self.ebit / self.total_assets
            + 0.6 * self.equity / self.total_liabilities
            + 1.0 * self.sales / self.total_assets
        )

    class PydanticMeta:
        computed = ("working_capital", "zscore")

    class Meta:
        table = "financial_year"


Report_Pydantic = pydantic_model_creator(Report, name="Report", exclude=("created_at", "updated_at"))
FinancialYear_Pydantic = pydantic_model_creator(FinancialYear, name="FinancialYear", exclude=("id",))
