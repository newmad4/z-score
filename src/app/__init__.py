import logging
import os
import sys

logging.getLogger(__name__)
logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format=f"[%(asctime)s] [{os.getpid()}] [%(levelname)s] %(message)s",
)
